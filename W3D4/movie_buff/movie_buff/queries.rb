# == Schema Information
#
# Table name: actors
#
#  id          :integer      not null, primary key
#  name        :string
#
# Table name: movies
#
#  id          :integer      not null, primary key
#  title       :string
#  yr          :integer
#  score       :float
#  votes       :integer
#  director_id :integer
#
# Table name: castings
#  id          :integer      not null, primary key
#  movie_id    :integer      not null
#  actor_id    :integer      not null
#  ord         :integer


def movie_names_before_1940
  # Find all the movies made before 1940. Show the id, title, and year.
  Movie.select("id", "title", "yr").where("yr < 1940")
end

def eighties_b_movies
	# List all the movies from 1980-1989 with scores falling between 3 and 5 (inclusive). Show the id, title, year, and score.
  Movie.select("id", "title", "yr", "score").where(score: 3..5, yr: 1980..1989)
end

def vanity_projects
  Movie.joins(:actors).select("movies.id", "movies.title", "actors.name AS name").where('movies.director_id = actors.id AND castings.ord = 1')
  # List the title of all movies in which the director also appeared as the starring actor. Show the movie id and title and director's name.

  # Note: Directors appear in the 'actors' table.

end

def starring(whazzername)
	# Find the movies with an actor who had a name like `whazzername`.
	# A name is like whazzername if the actor's name contains all of the letters in whazzername, ignoring case, in order.

	# ex. "Sylvester Stallone" is like "sylvester" and "lester stone" but not like "stallone sylvester" or "zylvester ztallone"
  search = whazzername.split(" ").join("").chars.join("%")
  Movie.joins(:actors).select("movies.*").where("LOWER(actors.name) LIKE LOWER('%#{search}%')").load
end

def bad_years
  # List the years in which a movie with a rating above 8 was not released.
  Movie.select("movies.yr").
    group("movies.yr").
    having("MAX(movies.score) < 8").map(&:yr)
end

def golden_age
	# Find the decade with the highest average movie score.

  Movie.select("movies.yr/10 AS decade")
    .group("decade")
    .order("AVG(movies.score) DESC")
    .limit(1)
    .first
    .decade*10
end

def cast_list(title)
  # List all the actors for a particular movie, given the title.  Sort the results by starring order (ord).
  Actor.joins(:movies).select("actors.id, actors.name").where("movies.title = ?", title).order("actors.name").load

end

def costars(name)
  # List the names of the actors that the named actor has ever appeared with.
  movies = Actor.find_by_name(name).movies
  movies.joins(:actors).select("DISTINCT actors.name").where("actors.name != ?", name).map(&:name)
end

def most_supportive
  # Find the two actors with the largest number of non-starring roles. Show each actor's id, name and number of supporting roles.
  Actor.joins(:castings)
    .select("actors.id", "actors.name", "COUNT(actors.id) AS roles")
    .where("castings.ord != 1")
    .group("actors.id")
    .order("COUNT(actors.id) DESC")
    .limit(2)
end

def what_was_that_one_with(those_actors)
	# Find the movies starring all `those_actors` (an array of actor names). Show each movie's title and id.
  Movie.joins(:actors)
    .select("movies.title", "movies.id").uniq
    .where("actors.name" => those_actors)
    .group("movies.id")
    .having("COUNT(movies.id) = #{those_actors.length}")
end

def actor_out_of_work
  # Find the number of actors in the database who have not appeared in a movie
  Actor.joins("LEFT OUTER JOIN castings ON castings.actor_id = actors.id")
    .where("castings.actor_id IS NULL").count
end

def longest_career
	#Find the actor and list all the movies of the actor who had the longest career (the greatest time between first and last movie).
  Actor.joins(:movies).
    select("actors.id", "actors.name", "MAX(movies.yr) - MIN(movies.yr) AS career").group("actors.id").order("career DESC, actors.name").limit(1).first
end
