# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  user_name  :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User < ActiveRecord::Base
  validates :user_name, presence: true, uniqueness: true

  has_many :responses,
    class_name: :Response,
    primary_key: :id,
    foreign_key: :user_id

  has_many :authored_polls,
    class_name: :Poll,
    primary_key: :id,
    foreign_key: :author_id

  def completed_polls
    Poll.find_by_sql([<<-SQL, self.id])
      SELECT
        polls.*
      FROM
        polls
      JOIN
        questions ON polls.id = questions.poll_id
      LEFT OUTER JOIN
        (
          SELECT
            answer_choices.question_id AS question_id,
            responses.id AS response_id
          FROM
            answer_choices
          JOIN
            responses ON responses.choice_id = answer_choices.id
          WHERE
            responses.user_id = 8
        ) AS choices_and_responses ON question_id = questions.id
      GROUP BY
        polls.id
      HAVING
        COUNT(response_id)/COUNT(questions.id) = 1
    SQL
  end


end
