# == Schema Information
#
# Table name: questions
#
#  id         :integer          not null, primary key
#  body       :text             not null
#  poll_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Question < ActiveRecord::Base
  validates :body, :poll_id, presence: true

  belongs_to :poll,
    class_name: :Poll,
    primary_key: :id,
    foreign_key: :poll_id

  has_many :answer_choices,
    class_name: :AnswerChoice,
    primary_key: :id,
    foreign_key: :question_id

  has_many :responses,
    through: :answer_choices,
    source: :responses

  def results
    counts = {}

    self.answer_choices
      .select("answer_choices.*", "COUNT(responses.id) AS response_count")
      .joins("LEFT OUTER JOIN responses ON responses.choice_id = answer_choices.id")
      .group("answer_choices.id")
      .map { |choice| counts[choice.text] = choice.response_count }

    counts
  end

  # def results
  #   choices = self.answer_choices.includes(:responses)
  #   counts = {}
  #   choices.map { |choice| counts[choice.text] = choice.responses.length }
  #
  #   counts
  # end

  # def better_results
  #   AnswerChoice.find_by_sql([<<-SQL, self.id])
  #     SELECT
  #       answer_choices.*, COUNT(responses.id) AS response_count
  #     FROM
  #       answer_choices
  #     LEFT OUTER JOIN
  #       responses ON responses.choice_id = answer_choices.id
  #     WHERE
  #       answer_choices.question_id = ?
  #     GROUP BY
  #       answer_choices.id
  #   SQL
  # end
end
