# == Schema Information
#
# Table name: responses
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  choice_id  :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Response < ActiveRecord::Base
  validates :user_id, :choice_id, presence: true
  validate :not_duplicate_response
  validate :respondent_is_not_author
  
  belongs_to :answer_choice,
    class_name: :AnswerChoice,
    primary_key: :id,
    foreign_key: :choice_id

  belongs_to :respondent,
    class_name: :User,
    primary_key: :id,
    foreign_key: :user_id

  has_one :question,
    through: :answer_choice,
    source: :question

  def sibling_responses
    self.question.responses.where.not(id: self.id)
  end

  def respondent_already_answered?
    sibling_responses.exists?(user_id: self.user_id)
  end

  def respondent_is_author?
    self.question.poll.author_id == self.user_id
  end

  private
  def not_duplicate_response
    if self.respondent_already_answered?
      errors[:base] << "You've already responded."
    end
  end

  def respondent_is_not_author
    if self.respondent_is_author?
      errors[:base] << "You can't respond to your own poll."
    end
  end

end
