# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.destroy_all
user1 = User.create!(user_name: "Ike")
user2 = User.create!(user_name: "WaltEvers")
user3 = User.create!(user_name: "Steve")
user4 = User.create!(user_name: "Huey")

Poll.destroy_all
poll1 = Poll.create!(title: "Rails survey", author_id: user1.id)

Question.destroy_all
question1 = Question.create!(body: "How do you feel about rails?", poll_id: poll1.id)
question2 = Question.create!(body: "How do you feel about the number of poll questions you received?", poll_id: poll1.id)

AnswerChoice.destroy_all
choice1 = AnswerChoice.create!(text: "Rails is great!", question_id: question1.id)
choice2 = AnswerChoice.create!(text: "Rails is meh.", question_id: question1.id)
choice3 = AnswerChoice.create!(text: "Far too many!", question_id: question2.id)
choice4 = AnswerChoice.create!(text: "Not nearly enough!", question_id: question2.id)

Response.destroy_all
Response.create!(user_id: user2.id, choice_id: choice2.id)
Response.create!(user_id: user3.id, choice_id: choice1.id)
Response.create!(user_id: user2.id, choice_id: choice4.id)
Response.create!(user_id: user3.id, choice_id: choice4.id)
Response.create!(user_id: user4.id, choice_id: choice4.id)
