require_relative 'db_connection'
require 'active_support/inflector'
require 'byebug'
# NB: the attr_accessor we wrote in phase 0 is NOT used in the rest
# of this project. It was only a warm up.

class SQLObject
  
  def self.columns
    return @columns if @columns
    cols ||= DBConnection.execute2(<<-SQL).first
      SELECT
        *
      FROM
        "#{table_name}"
      LIMIT
        0
    SQL

    cols.map!(&:to_sym)
    @columns = cols
  end

  def self.finalize!

    self.columns.each do |col|
      define_method(col) {
        attributes[col]
      }
      define_method("#{col}=") { |vals|
        attributes[col] = vals
      }
    end

  end

  def self.table_name=(table_name)
    @table = table_name.to_s.tableize
  end

  def self.table_name
    @table ? @table : self.to_s.tableize
  end

  def self.all
    cat_arrs = DBConnection.execute(<<-SQL)
      SELECT
        *
      FROM
        #{table_name}
    SQL

    self.parse_all(cat_arrs)
  end

  def self.parse_all(results)
    res = []
    results.each do |tbl_row|
      res << self.new(tbl_row)
    end
    res
  end

  def self.find(id)
    found_id = DBConnection.execute(<<-SQL, id)
      SELECT
        *
      FROM
        #{table_name}
      WHERE
        id = ?
    SQL

    self.parse_all(found_id).first
    # ...
  end

  def initialize(params = {})
    params.each do |k,v|
      raise "unknown attribute '#{k}'" unless self.class.columns.include?(k.to_sym)
      self.send("#{k}=",v)
    end
  end

  def attributes
    @attributes ||= {}
  end

  def attribute_values
    self.class.columns.map do |attribute|
      self.send(attribute)
    end
    # ...
  end

  def insert
    # ...
    colnames = self.class.columns
    col_names = colnames.join(", ")
    question_marks = (["?"] * colnames.length).join(", ")

    DBConnection.execute(<<-SQL, *attribute_values)
      INSERT INTO
        #{self.class.table_name} ( #{col_names} )
      VALUES
        ( #{question_marks} )
    SQL

    self.id = DBConnection.last_insert_row_id
  end

  def update
    colnames = self.class.columns
    col_names = colnames.join(", ")
    question_marks = colnames.map do |one_name|
      "#{one_name} = ?"
    end.join(", ")

    DBConnection.execute(<<-SQL, *attribute_values, *self.id)
      UPDATE
        #{self.class.table_name}
      SET
        #{question_marks}
      WHERE
        id = ?
    SQL
  end

  def save
    # ...
    if id.nil?
      self.insert
    else
      self.update
    end
  end
end
