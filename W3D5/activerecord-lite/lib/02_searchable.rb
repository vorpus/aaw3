require_relative 'db_connection'
require_relative '01_sql_object'

module Searchable
  def where(params)
    where_line = params.keys.map do |k|
      "#{k} = ?"
    end.join(" AND ")
    rerun = DBConnection.execute(<<-SQL, *params.values)
      SELECT
        *
      FROM
        #{table_name}
      WHERE
        #{where_line}
    SQL

    rerun.map! do |cat_attrs|
      self.new(cat_attrs)
    end
  end
end

class SQLObject
  extend Searchable
  # Mixin Searchable here...
end
