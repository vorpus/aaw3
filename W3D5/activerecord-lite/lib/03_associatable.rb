require_relative '02_searchable'
require 'active_support/inflector'

# Phase IIIa
class AssocOptions
  attr_accessor(
    :foreign_key,
    :class_name,
    :primary_key
  )

  def model_class
    self.class_name.constantize
    # ...
  end

  def table_name
    # ...
    self.class_name.downcase+"s"
  end
end

class BelongsToOptions < AssocOptions
  def initialize(name, options = {})

    options[:primary_key] ||= :id
    @primary_key = options[:primary_key]

    options[:foreign_key] ||= "#{name}_id".to_sym
    @foreign_key = options[:foreign_key]

    options[:class_name]  ||= "#{name}".split("_").map(&:capitalize).join("")
    @class_name = options[:class_name]

  end


end


# Human
# has_many :cats
#   class_name: :Cats
#   primary_key: :id
#   foreign_key: :human_id

class HasManyOptions < AssocOptions
  def initialize(name, self_class_name, options = {})
    # ...
    options[:primary_key] ||= :id
    @primary_key = options[:primary_key]

    options[:foreign_key] ||= "#{self_class_name.downcase}_id".to_sym
    @foreign_key = options[:foreign_key]

    options[:class_name]  ||= "#{name}".split("_").map(&:capitalize).join("").singularize
    @class_name = options[:class_name]
  end
end



module Associatable
  # Phase IIIb
  def belongs_to(name, options = {})
    # ...
    assoc_options
    btoptions = BelongsToOptions.new(name, options)
    @assoc_options[name] = btoptions
    define_method(name) do
      fk = self.send(btoptions.foreign_key)
      mc = btoptions.model_class
      found = mc.where(id: fk).first
    end
  end

  def has_many(name, options = {})
    # ...
    hmoptions = HasManyOptions.new(name, self.to_s, options)

    define_method(name) do
      pk = self.send(hmoptions.primary_key)
      hmoptions.model_class.where(hmoptions.foreign_key => pk)
    end
  end

  def assoc_options
    # Wait to implement this in Phase IVa. Modify `belongs_to`, too.
    @assoc_options ||= {}
  end
end

class SQLObject
  extend Associatable
  # Mixin Associatable here...
end
