class AttrAccessorObject
  def self.my_attr_accessor(*names)
    names.each do |attrs|
      define_method("#{attrs}") {
        self.instance_variable_get("@#{attrs}")
      }
      define_method("#{attrs}=") { |vals|
        self.instance_variable_set("@#{attrs}", vals)
      }
    end
  end
end
