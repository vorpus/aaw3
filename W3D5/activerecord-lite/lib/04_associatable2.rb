require_relative '03_associatable'

# Phase IV
module Associatable
  # Remember to go back to 04_associatable to write ::assoc_options

  def has_one_through(name, through_name, source_name)
    # ...

    through_options = @assoc_options[through_name]

    define_method(name) do
      source_options = through_options.model_class.assoc_options[source_name]

       through_foreign = self.send(through_options.foreign_key)
      through_item = through_options.class_name.constantize.where(id: through_foreign).first
      wtf = through_item.send(source_options.foreign_key)
      source_options.model_class.where(source_options.primary_key => wtf).first
    end
  end
end
