require 'byebug'
class Models

  def self.all
    data = QuestionsDatabase.instance.execute("SELECT * FROM #{self.table_name}")
    data.map { |datum| self.new(datum) }
  end

  def self.find_by_id(id)
    datum = QuestionsDatabase.instance.execute(<<-SQL, id)
      SELECT
        *
      FROM
        #{self.table_name}
      WHERE
        id = ?
    SQL
    self.new(datum.first)
  end

  def save
    debugger
    args = self.extract_args
    vars = extract_vars_from_args(args)

    if @id
      QuestionsDatabase.instance.execute(<<-SQL, *vars, @id)
        UPDATE
          #{self.class.table_name}
        SET
          #{create_sets(args)}
        WHERE
          id = ?
      SQL
    else
      QuestionsDatabase.instance.execute(<<-SQL, *vars)
        INSERT INTO
          #{self.class.table_name} #{create_insert(args)}
        VALUES
          #{create_values(args)}
      SQL
      @id = QuestionsDatabase.instance.last_insert_row_id
    end
  end

  def self.where(options)
    data = QuestionsDatabase.instance.execute(<<-SQL )
      SELECT
        *
      FROM
        #{self.table_name}
      WHERE
        #{self.where_args(options)}
    SQL

    data.map { |datum| self.new(datum) }
  end

  def self.where_args(options)
    where = ""
    options.each { |k, v| where += "#{k} = '#{v}' AND " }
    where[0...-4]
  end

  def extract_args
    self.instance_variables.map(&:to_s).drop(1)
  end

  def create_values(args)
    "(" + args.inject("") { |sum, x| sum + "?, "}[0...-2] + ")"
  end

  def create_sets(args)
    args.inject("") { |sum, x| sum + x + " = ?, " }[0...-2].gsub("@","")
  end

  def create_insert(args)
    "(" + args.inject("") { |sum, x| sum + x + ", "}[0...-2].gsub("@","") + ")"
  end

  def extract_vars_from_args(args)
    args.map { |arg| self.send(arg.gsub("@","")) }
  end
end
