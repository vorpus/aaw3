class Questions < Models
  attr_accessor :title, :body, :author_id

  # def self.all
  #   data = QuestionsDatabase.instance.execute("SELECT * FROM questions")
  #   data.map { |datum| Questions.new(datum) }
  # end

  def initialize(options)
    @id = options['id']
    @title = options['title']
    @body = options['body']
    @author_id = options['author_id']
  end

  def self.table_name
    'questions'
  end

  # def self.find_by_id(id)
  #   datum = QuestionsDatabase.instance.execute(<<-SQL, id)
  #     SELECT
  #       *
  #     FROM
  #       questions
  #     WHERE
  #       id = ?
  #   SQL
  #   Questions.new(datum.first)
  # end

  # def save
  #   if @id
  #     QuestionsDatabase.instance.execute(<<-SQL, @title, @body, @author_id, @id)
  #       UPDATE
  #         questions
  #       SET
  #         title = ?, body = ?, author_id = ?
  #       WHERE
  #         id = ?
  #     SQL
  #   else
  #     QuestionsDatabase.instance.execute(<<-SQL, @title, @body, @author_id)
  #       INSERT INTO
  #         questions (title, body, author_id)
  #       VALUES
  #         (?, ?, ?)
  #     SQL
  #     @id = QuestionsDatabase.instance.last_insert_row_id
  #   end
  # end

  def self.find_by_author_id(author_id)
    data = QuestionsDatabase.instance.execute(<<-SQL, author_id)
      SELECT
        *
      FROM
        questions
      WHERE
        author_id = ?
    SQL
    data.map { |datum| Questions.new(datum) }
  end

  def author
    Users.find_by_id(@author_id)
  end

  def replies
    Replies.find_by_question_id(@id)
  end

  def followers
    QuestionFollows.followers_for_question_id(@id)
  end

  def most_followed(n = 3)
    QuestionFollows.most_followed_questions(n)
  end

  def likers
    QuestionLikes.likers_for_question_id(@id)
  end

  def num_likes
    QuestionLikes.num_likes_for_question_id(@id)
  end

  def most_liked(n)
    QuestionLikes.most_liked_questions(n)
  end
end
