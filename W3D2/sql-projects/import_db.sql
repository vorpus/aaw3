DROP TABLE IF EXISTS users;

CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  fname TEXT NOT NULL,
  lname TEXT NOT NULL
);

DROP TABLE if exists questions;

CREATE TABLE questions (
  id INTEGER PRIMARY KEY,
  title TEXT NOT NULL,
  body TEXT,
  author_id INTEGER NOT NULL,

  FOREIGN KEY (author_id) REFERENCES users(id)
);

DROP TABLE if exists question_follows;

CREATE TABLE question_follows (
  id INTEGER PRIMARY KEY,
  user_id INTEGER NOT NULL,
  question_id INTEGER NOT NULL,

  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (question_id) REFERENCES questions(id)
);


DROP TABLE if exists replies;

CREATE TABLE replies (
  id INTEGER PRIMARY KEY,
  user_id INTEGER NOT NULL,
  parent_reply INTEGER,
  question_id INTEGER NOT NULL,
  body TEXT NOT NULL,

  FOREIGN KEY (parent_reply) REFERENCES replies(id),
  FOREIGN KEY (question_id) REFERENCES questions(id),
  FOREIGN KEY (user_id) REFERENCES users(id)
);

DROP TABLE if exists question_likes;

CREATE TABLE question_likes (
  id INTEGER PRIMARY KEY,
  user_id INTEGER NOT NULL,
  question_id INTEGER NOT NULL,

  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (question_id) REFERENCES questions(id)
);

INSERT INTO
  users (fname, lname)
VALUES
  ('Chef', 'Boyardee'),
  ('Fitty', 'Cent'),
  ('Flava', 'Flav');

INSERT INTO
  questions (title, body, author_id)
VALUES
  ('What''s a query?', 'What is a query?', (SELECT id FROM users WHERE fname = 'Fitty')),
  ('Length of program', 'How long does app academy take?', (SELECT id FROM users WHERE fname = 'Flava')),
  ('abcde', 'netntry?', (SELECT id FROM users WHERE fname = 'Fitty')),
  ('abcde5124', 'Whaetn5564 a query?', (SELECT id FROM users WHERE fname = 'Fitty')),
  ('sderb', 'What 205275273y?', (SELECT id FROM users WHERE fname = 'Fitty')),
  ('35h3h3h', 'What22234uery?', (SELECT id FROM users WHERE fname = 'Fitty')),
  ('Cinnamon Toast Crunch', 'Why do kids love it?', (SELECT id FROM users WHERE fname = 'Chef'));

INSERT INTO
  question_follows (user_id, question_id)
VALUES
  ((SELECT id FROM users WHERE fname = 'Fitty'), (SELECT id FROM questions WHERE title = 'Length of program')),
  ((SELECT id FROM users WHERE fname = 'Chef'), (SELECT id FROM questions WHERE title = 'What''s a query?')),
  ((SELECT id FROM users WHERE fname = 'Fitty'), (SELECT id FROM questions WHERE title = 'What''s a query?'));

INSERT INTO
  replies (user_id, question_id, parent_reply, body)
VALUES
  ((SELECT id FROM users WHERE fname = 'Fitty'), (SELECT id FROM questions WHERE title = 'What''s a query?'), NULL, 'First'),
  ((SELECT id FROM users WHERE fname = 'Flava'), (SELECT id FROM questions WHERE title = 'What''s a query?'), 1, 'Second');

INSERT INTO
  question_likes (user_id, question_id)
VALUES
  ((SELECT id FROM users WHERE fname = 'Fitty'), (SELECT id FROM questions WHERE title = 'What''s a query?')),
  ((SELECT id FROM users WHERE fname = 'Flava'), (SELECT id FROM questions WHERE title = 'What''s a query?')),
  ((SELECT id FROM users WHERE fname = 'Chef'), (SELECT id FROM questions WHERE title = 'abcde5124')),
  ((SELECT id FROM users WHERE fname = 'Chef'), (SELECT id FROM questions WHERE title = 'What''s a query?'));
