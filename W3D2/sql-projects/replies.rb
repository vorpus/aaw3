class Replies < Models
  attr_accessor :user_id, :parent_reply, :question_id, :body

  def initialize(options)
    @id = options['id']
    @user_id = options['user_id']
    @parent_reply = options['parent_reply']
    @question_id = options['question_id']
    @body = options['body']
  end

  def self.table_name
    'replies'
  end

  # def save
  #   if @id
  #     QuestionsDatabase.instance.execute(<<-SQL, @user_id, @parent_reply, @question_id, @body, @id)
  #       UPDATE
  #         replies
  #       SET
  #         user_id = ?, parent_reply = ?, question_id = ?, body = ?
  #       WHERE
  #         id = ?
  #     SQL
  #   else
  #     QuestionsDatabase.instance.execute(<<-SQL, @user_id, @parent_reply, @question_id, @body)
  #       INSERT INTO
  #         replies (user_id, parent_reply, question_id, body)
  #       VALUES
  #         (?, ?, ?, ?)
  #     SQL
  #     @id = QuestionsDatabase.instance.last_insert_row_id
  #   end
  # end

  def self.find_by_user_id(user_id)
    datum = QuestionsDatabase.instance.execute(<<-SQL, user_id)
      SELECT
        *
      FROM
        replies
      WHERE
        user_id = ?
    SQL
    Replies.new(datum.first)
  end

  def self.find_by_question_id(question_id)
    data = QuestionsDatabase.instance.execute(<<-SQL, question_id)
      SELECT
        *
      FROM
        replies
      WHERE
        question_id = ?
    SQL
    data.map { |datum| Replies.new(datum) }
  end

  def author
    Users.find_by_id(@user_id)
  end

  def question
    Questions.find_by_id(@question_id)
  end

  def parent_reply
    return nil if @parent_reply.nil?
    Replies.find_by_id(@parent_reply)
  end

  def child_replies
    data = QuestionsDatabase.instance.execute(<<-SQL, @id)
      SELECT
        *
      FROM
        replies
      WHERE
        parent_reply = ?
    SQL
    return nil if data.empty?
    data.map { |datum| Replies.new(datum) }
  end
end
