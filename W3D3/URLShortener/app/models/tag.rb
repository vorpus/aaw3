# == Schema Information
#
# Table name: tags
#
#  id               :integer          not null, primary key
#  shortened_url_id :integer          not null
#  tag              :string           not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Tag < ActiveRecord::Base
  validates :shortened_url_id, :tag, :presence => true
  validates_inclusion_of :tag, in: ['Sports', 'Humor', 'Philosophy', 'Cats']

  belongs_to :shortened_url,
    class_name: :ShortenedUrl,
    primary_key: :id,
    foreign_key: :shortened_url_id

  has_many :visits,
    through: :shortened_url
    source: :visits

  def self.popular_links(tag)
    urls = []
    links = Tag.find_by_tag(tag)
    links.each do |link|
      urls << ShortenedUrl.find_by_id(link.id)
    end

    urls.sort! { |x,y| y.visits <=> x.visits }

  end

  def self.tag(user, shortened_url)
    self.create("user_id"=>user.id, "shortened_url_id" => shortened_url.id)
  end

end
