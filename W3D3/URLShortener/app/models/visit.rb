# == Schema Information
#
# Table name: visits
#
#  id               :integer          not null, primary key
#  user_id          :integer          not null
#  shortened_url_id :integer          not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Visit < ActiveRecord::Base
  validates :user_id, :shortened_url_id, :presence => true

  belongs_to :visitors,
    class_name: :User,
    primary_key: :id,
    foreign_key: :user_id

  belongs_to :shortened_urls,
    class_name: :ShortenedUrl,
    primary_key: :id,
    foreign_key: :shortened_url_id

  def self.record_visit!(user,shortened_url)
    short_id = ShortenedUrl.find_by_short_url(shortened_url).id
    self.create("user_id"=>user.id, "shortened_url_id"=>short_id)
  end

end
