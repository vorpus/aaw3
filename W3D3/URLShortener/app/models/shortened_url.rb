# == Schema Information
#
# Table name: shortened_urls
#
#  id           :integer          not null, primary key
#  long_url     :string           not null
#  short_url    :string           not null
#  submitter_id :integer          not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class ShortenedUrl < ActiveRecord::Base
  validates :long_url, :submitter_id, :presence => true
  validates :short_url, :presence => true, :uniqueness => true

  belongs_to :submitter,
    class_name: :User,
    primary_key: :id,
    foreign_key: :submitter_id

  has_many :visits,
    class_name: :Visit,
    primary_key: :id,
    foreign_key: :shortened_url_id

  has_many :visitors,
    Proc.new { distinct },
    through: :visits,
    source: :visitors

  def num_clicks
    people = self.visits.count
  end

  def num_uniques
    self.visitors.select(:user_id).count
  end

  def num_recent_uniques
    dv = self.visits.where("created_at > '#{30.minutes.ago}'").distinct
    # Visit.all.where("created_at > '#{30.minutes.ago}'")
  end

  def self.random_code
    rand_short = SecureRandom::urlsafe_base64
    until !exists?(:short_url => rand_short)
      rand_short = SecureRandom::urlsafe_base64
    end
    rand_short
  end

  def self.create_for_user_and_long_url!(user, long_url)
    new_short = self.create({
      :submitter_id => user.id,
      :short_url => self.random_code,
      :long_url => long_url
    })
  end




end
