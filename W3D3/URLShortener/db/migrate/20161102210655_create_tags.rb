class CreateTags < ActiveRecord::Migration[5.0]
  def change
    create_table :tags do |t|
      t.integer :shortened_url_id, null: false
      t.string :tag, null: false

      t.timestamps
    end

    add_index(:tags, :shortened_url_id)
    add_index(:tags, :tag)
  end
end
